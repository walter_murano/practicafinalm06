(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-ficha-ficha-module"],{

/***/ "CEpp":
/*!*****************************************************!*\
  !*** ./src/app/pages/ficha/ficha-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: FichaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FichaPageRoutingModule", function() { return FichaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ficha_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ficha.page */ "zTo9");




const routes = [
    {
        path: '',
        component: _ficha_page__WEBPACK_IMPORTED_MODULE_3__["FichaPage"]
    }
];
let FichaPageRoutingModule = class FichaPageRoutingModule {
};
FichaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FichaPageRoutingModule);



/***/ }),

/***/ "Vzm9":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/ficha/ficha.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Detalle moto</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<ion-card>\n  <ion-header>\n    <ion-toolbar>\n      <ion-card-subtitle>{{motos.marca}} {{motos.modelo}}</ion-card-subtitle>\n      <ion-title>{{motos.marca}} {{motos.modelo}}</ion-title>\n    </ion-toolbar>\n  </ion-header>\n  <ion-card-content>\n    <img [src]=\"motos.foto\" />\n    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Neque illo ipsam vitae deserunt dolorem qui doloribus, dolor animi provident cum fugiat \n      amet eaque ullam officiis? Provident quibusdam fuga dignissimos, repellat enim necessitatibus reiciendis expedita hic quis ut rerum, mollitia sed\n       magni explicabo laborum? Error assumenda dolorem sint tenetur nam non!</p>\n       <ion-button (click)=\"eliminar()\" expand=\"block\" color=\"danger\" shape=\"round\">\n         Eliminar\n       </ion-button>\n  </ion-card-content>\n</ion-card>\n<ion-button (click)=\"volver()\" expand=\"block\" shape=\"round\" color=\"warning\">\n  Volver\n</ion-button>\n</ion-content>\n");

/***/ }),

/***/ "YtXk":
/*!*********************************************!*\
  !*** ./src/app/pages/ficha/ficha.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmaWNoYS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "lazA":
/*!*********************************************!*\
  !*** ./src/app/pages/ficha/ficha.module.ts ***!
  \*********************************************/
/*! exports provided: FichaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FichaPageModule", function() { return FichaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _ficha_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ficha-routing.module */ "CEpp");
/* harmony import */ var _ficha_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ficha.page */ "zTo9");







let FichaPageModule = class FichaPageModule {
};
FichaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ficha_routing_module__WEBPACK_IMPORTED_MODULE_5__["FichaPageRoutingModule"]
        ],
        declarations: [_ficha_page__WEBPACK_IMPORTED_MODULE_6__["FichaPage"]]
    })
], FichaPageModule);



/***/ }),

/***/ "zTo9":
/*!*******************************************!*\
  !*** ./src/app/pages/ficha/ficha.page.ts ***!
  \*******************************************/
/*! exports provided: FichaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FichaPage", function() { return FichaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_ficha_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./ficha.page.html */ "Vzm9");
/* harmony import */ var _ficha_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ficha.page.scss */ "YtXk");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");






let FichaPage = class FichaPage {
    constructor(route, router, alertController) {
        this.route = route;
        this.router = router;
        this.alertController = alertController;
        this.route.queryParams.subscribe(params => {
            this.motos = JSON.parse(params.datos);
            this.index = params.index;
        });
    }
    ngOnInit() {
    }
    volver() {
        this.router.navigate(['home']);
    }
    presentAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Alert',
                message: 'Estas seguro de querer eliminarlo',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        handler: () => {
                            //accion a hacer
                        }
                    },
                    {
                        text: 'Aceptar',
                        handler: () => {
                            const url = "http://carlos-plaza-7e3.alwaysdata.net/miapi/motoElimin/" + this.motos.id;
                            fetch(url, {
                                "method": "DELETE"
                            })
                                .then(response => {
                                this.router.navigateByUrl('/home');
                                this.conseguido();
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    conseguido() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Borrado correctamente',
                buttons: ['OK']
            });
            yield alert.present();
        });
    }
    eliminar() {
        this.presentAlert();
    }
};
FichaPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] }
];
FichaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-ficha',
        template: _raw_loader_ficha_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_ficha_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], FichaPage);



/***/ })

}]);
//# sourceMappingURL=pages-ficha-ficha-module.js.map