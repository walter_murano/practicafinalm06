(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-altamoto-altamoto-module"],{

/***/ "RsRV":
/*!*************************************************!*\
  !*** ./src/app/pages/altamoto/altamoto.page.ts ***!
  \*************************************************/
/*! exports provided: AltamotoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AltamotoPage", function() { return AltamotoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_altamoto_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./altamoto.page.html */ "nXoX");
/* harmony import */ var _altamoto_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./altamoto.page.scss */ "ZoWi");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let AltamotoPage = class AltamotoPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    altaMoto() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.image = document.getElementsByName("foto")[0].files[0];
            var formData = new FormData();
            formData.append('marca', this.marca);
            formData.append('modelo', this.modelo);
            formData.append('year', this.ano);
            formData.append('foto', this.image);
            formData.append('precio', this.precio.toString());
            console.log(formData);
            fetch("http://carlos-plaza-7e3.alwaysdata.net/miapi/profile", {
                "method": "POST",
                "body": formData
            })
                .then(response => {
                this.router.navigateByUrl('/home');
            });
        });
    }
    volver() {
        this.router.navigate(['home']);
    }
};
AltamotoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
AltamotoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-altamoto',
        template: _raw_loader_altamoto_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_altamoto_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AltamotoPage);



/***/ }),

/***/ "ZoWi":
/*!***************************************************!*\
  !*** ./src/app/pages/altamoto/altamoto.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#foto {\n  display: none;\n}\n\n#icon {\n  width: 100%;\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2FsdGFtb3RvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGFBQUE7QUFDRDs7QUFDQTtFQUNDLFdBQUE7RUFDQSxlQUFBO0FBRUQiLCJmaWxlIjoiYWx0YW1vdG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2ZvdG97XHJcbiBkaXNwbGF5OiBub25lO1xyXG4gfVxyXG4jaWNvbntcclxuIHdpZHRoOiAxMDAlO1xyXG4gY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "eF6+":
/*!***************************************************!*\
  !*** ./src/app/pages/altamoto/altamoto.module.ts ***!
  \***************************************************/
/*! exports provided: AltamotoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AltamotoPageModule", function() { return AltamotoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _altamoto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./altamoto-routing.module */ "nZ2f");
/* harmony import */ var _altamoto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./altamoto.page */ "RsRV");







let AltamotoPageModule = class AltamotoPageModule {
};
AltamotoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _altamoto_routing_module__WEBPACK_IMPORTED_MODULE_5__["AltamotoPageRoutingModule"]
        ],
        declarations: [_altamoto_page__WEBPACK_IMPORTED_MODULE_6__["AltamotoPage"]]
    })
], AltamotoPageModule);



/***/ }),

/***/ "nXoX":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/altamoto/altamoto.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<link href=\"https://use.fontawesome.com/releases/v5.0.4/css/all.css\" rel=\"stylesheet\">\n<ion-header>\n  <ion-toolbar>\n    <ion-title>Nueva moto</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-item>\n    <label for=\"foto\">\n      <i class=\"fas fa-camera fa-10x\" id=\"icon\" ></i>\n    </label>\n    <input id=\"foto\" type=\"file\" name=\"foto\" accept=\"image/*\" capture=\"camera\" />\n  </ion-item>\n\n  <ion-item>\n    <ion-label>Marca:</ion-label>\n    <ion-input [(ngModel)]=\"marca\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label>Modelo:</ion-label>\n    <ion-input [(ngModel)]=\"modelo\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label>Año:</ion-label>\n    <ion-input [(ngModel)]=\"ano\"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label>Precio:</ion-label>\n    <ion-input [(ngModel)]=\"precio\" type=\"number\"></ion-input>\n  </ion-item>\n\n\n<ion-button (click)=\"altaMoto()\" expand=\"block\" color=\"success\"shape=\"round\">\n  Añadir\n</ion-button>\n\n<ion-button (click)=\"volver()\" expand=\"block\" color=\"warning\" shape=\"round\">\n  Volver\n</ion-button>\n</ion-content>\n\n");

/***/ }),

/***/ "nZ2f":
/*!***********************************************************!*\
  !*** ./src/app/pages/altamoto/altamoto-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: AltamotoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AltamotoPageRoutingModule", function() { return AltamotoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _altamoto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./altamoto.page */ "RsRV");




const routes = [
    {
        path: '',
        component: _altamoto_page__WEBPACK_IMPORTED_MODULE_3__["AltamotoPage"]
    }
];
let AltamotoPageRoutingModule = class AltamotoPageRoutingModule {
};
AltamotoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AltamotoPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-altamoto-altamoto-module.js.map